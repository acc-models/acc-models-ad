! Based on Pavel's Survey_preparation.madx file, but with major modifications:
!
! - calling local files instead of Pavel's public
! - calling also ../AD_PUs/AD_PUs where to find monitor definitions 
! - removed un-necessary lines about TWISS
! - "standardised" survey output
! - Using Init condition given by Dominique Missiaen on 4th April 2019
! - Removed all (almost) markers for output on request of Dominique
!
! Davide August 1th 2019

call,file= "../ad.seq";
! Un comment the following if we want to split quads and add BPMs
!call,file= "../tools/minimum_split_and_add_BPMs.madx";

BEAM,PARTICLE=ELECTRON;
USE,SEQUENCE=AD;

! Edit AD sequence for official survey purposes !!!!!!!!!!!!!!!!!!
! 1) cycle the sequence to start at the entrance of DR.QDS01
! 2) Remove all markers as requested by Dominique - 1/08/2019
! 3) install standard begin/end markers according to Ilias's standards
DR.BEGIN: MARKER;
DR.END:   MARKER;

SEQEDIT,sequence=AD;
FLATTEN;
CYCLE, start=DR.STARTSURVEY;
! remove half DR.QDS01 elements and replace by single quad element
SELECT,   FLAG=SEQEDIT, PATTERN=DR.QDS01.*;
REMOVE,   ELEMENT=SELECTED;
REMOVE,   ELEMENT=DR.START.AD;
REMOVE,   ELEMENT=DR.END.AD;
REMOVE,   ELEMENT=ad$start;
REMOVE,   ELEMENT=ad$end;
INSTALL,  ELEMENT=DR.QDS01, at = 0.7106/2;
!
SELECT,   FLAG=SEQEDIT, CLASS=MARKER;
REMOVE,   ELEMENT=SELECTED;
!
INSTALL,  ELEMENT=DR.BEGIN,  at = 0;
INSTALL,  ELEMENT=DR.END,    at = 182.432800;
ENDEDIT;
USE, SEQUENCE=AD;
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! The following are GEODE coordinates - being converted into MAD-X coordinate at SURVEY command call.
! +13 mm on ZZ0 has been added already.
! New start point (as before) is begin of Q1, i.e. 0.7106/2 earlier than what given below...
!   -> corrected only for pavel inputs
!
!! Values provided on April 4th 2019 by Dominique Missiaen + iterations on THETA0
! THETA0 =     -1.111603828*200/PI;
! PHI0   =     -0.00004108; 
! PSI0   =      0.; 
! XX0    =   1730.998365 - 0.7106/2*cos(PHI0)*sin(THETA0*PI/200);
! YY0    =   2144.221230 - 0.7106/2*cos(PHI0)*cos(THETA0*PI/200);
! ZZ0    =   2436.145885 - 0.7106/2*sin(PHI0);
!!
!! Values used by Pavel - maybe outdated, but necessary for testing
! THETA0 =     (-1.111603828+0.00)*200/PI;
! PHI0   =     -0.00004134+0.0000005; 
! PSI0   =     -0.0000080 - 0.0000002; 
! XX0    =   1730.998365 - 0.7106/2*cos(PHI0)*sin(THETA0*PI/200);
! YY0    =   2144.22123  - 0.7106/2*cos(PHI0)*cos(THETA0*PI/200);
! ZZ0    =   2436.1459   - 0.7106/2*sin(PHI0);
!! Pavel's format - outdated, but necessary for testing
! SET,FORMAT="20.9f";
!
!! Values merged between Pavel and Dominique. Start of Q01.
!! With correction to best match what is in Geode
! THETA0 =     (-1.111603828)*200/PI;
! PHI0   =     -0.00004108; 
! PSI0   =     -0.000008;
! XX0    =   1730.998365 - 0.7106/2*cos(PHI0)*sin(THETA0*PI/200);
! YY0    =   2144.22123  - 0.7106/2*cos(PHI0)*cos(THETA0*PI/200);
! ZZ0    =   2436.145885 - 0.7106/2*sin(PHI0);
!
!
!! Values used by Olav
! THETA0 =    -1.11160382800*200/PI ;
! PHI0   =    -0.00004134; 
! PSI0   =    -0.000008;
! XX0    =  1730.99836528162 ;
! YY0    =  2144.22123 ;
! ZZ0    =  2436.145905;
!
!! Values from GEODE
! THETA0 =   -1.11160430560958919819*200/PI; ! i.e. 329.2330720
! PHI0   =    -0.000041880; 
! PSI0   =     0.;
! XX0    =  1731.316860 ;
! YY0    =  2144.063750;
! ZZ0    =  2436.132920;



!! Final values sent to Dominique 17 May 2019 @22:00
 THETA0 =    -70.766897594;
 PHI0   =     -0.000041080; 
 PSI0   =     -0.000008500;
 XX0    =   1731.316859719;
 YY0    =   2144.063752371;
 ZZ0    =   2436.145899596; 


!
! set standard output format:
title, "AD Ring (DR)";
set,  format="15.9f";
select, flag=survey,clear;
!
! STANDARD:
!select, flag=survey, column=NAME,S,L,ANGLE,X,Y,Z,THETA,PHI,PSI,GLOBALTILT,SLOT_ID, ASSEMBLY_ID;
! added KEYWORD
select, flag=survey, column=NAME,KEYWORD,S,L,ANGLE,X,Y,Z,THETA,PHI,PSI,GLOBALTILT,SLOT_ID, ASSEMBLY_ID;

!
! Calling survey with correct conventions
survey, x0= -XX0, y0= ZZ0, z0= YY0,
      , theta0= -THETA0*PI/200, phi0=PHI0, psi0=PSI0
      , file="./AD_input_for_GEODE.sur";

!
! A simple survey for internal use only:
survey, file="./AD_0.sur";

STOP;

