---
template: overrides/main.html
---

AD Ring Optics
===

This [repository](https://gitlab.cern.ch/acc-models/acc-models-ad) contains the model of AD made merging the main files of the previous model from Olav (see [link](http://cern-accelerators-optics.web.cern.ch/cern-accelerators-optics/AD/defaultAD.htm)
and the one for Pavel, coming from his public/ad/madx AFS folder.

##  Optics

AD has **two optics**, one at **high energy** optimized to maximize acceptance (order of 200 um geometric acceptance at injection) and to fit stochastic cooling constraints, and another at **low energy** optimized for e-cooling.
The optics change happens after the second plateau, just before deceleration to 300 MeV/c.
Probably, the main reference with (early) description of AD optics is the following:
> *Beloshitsky, P et al.* Optics for the Antiproton Decelerator at CERN - [CERN-PS-2001-042-AE](https://cds.cern.ch/record/507408) - June 2001


### High Energy 
> p = 3.575 and 2.0 GeV/c
> Note that in the plot below, dispersion is computed with DX from MAD-X * beta_rel (=0.9672 for 3.575 GeV/c).


TFS table of the optics available [here](scenarios/highenergy/highenergy.tfs){target=_blank}.
<object width="100%" height="550" data="scenarios/highenergy/highenergy.html"></object> 

### Low Energy 
> p = 300 and 100 MeV/c
> Note that in the plot below, dispersion is computed with DX from MAD-X * beta_rel (=0.3045 for 300 MeV/c).

TFS table of the optics available [here](scenarios/lowenergy/lowenergy.tfs){target=_blank}.
<object width="100%" height="550" data="scenarios/lowenergy/lowenergy.html"></object> 


## MAD-X Example on SWAN

You can directly open a MAD-X example in [SWAN](https://cern.ch/swanserver/cgi-bin/go?projurl=file://eos/project/a/acc-models/public/ad/MADX_example_ad.ipynb){target=_blank}.

## Additional notes and references

- Optics has been left basically unchanged since 2009, when most effort was put in the design of ELENA ring.
- QMAIN1:
   - Most quadrupoles but a few around the electron cooler
   - moves both tunes (2xH; 1xV)
- QMAIN2:
   - moves mainly the vertical tune (1x)
- **DR.QDC.53** and **DR.QFC.54** are special quadrupoles: they have the same integrated strength as the other QFN/QDF magnets, but only at injection energy! At lower energies, they ave about 3% (DR.QDC.53) and 30% (DR.QFC.54) strength differences. This was compensated by special trim power supplies (**TODO: check which ones**)
- Due to saturation and/or limitation on power supply current, the optics is asymmetric at high energy.
- At low energy, coupling due to solenoidal field of the e-cooler starts to become relevant.
- According to Pavel (if I understood correctly), ideas to improve:
   - Actually better to keep a small energy step between 3rd and 4th plateau as now (300 > 100 MeV/c)
   - Should improve quality of EC solenoid to have straightness better than 10^-4
- Strength files were copied from Pavel's public:
```
cp /afs/cern.ch/user/b/beloshit/public/ad/madx/AD_quad_HEstrengths HE-optics.str
cp /afs/cern.ch/user/b/beloshit/public/ad/madx/ad.quads.LEmodelffe_for_madx LE-optics.str
```
- There are (undocumented?!) sextupole windings on quads 01, 03, 15, 43, 55. Those are in series with the circuit of the corresponding quad.
- The reference for circuit diagram is EDSM [#1842343](https://edms.cern.ch/ui/file/1842343/1/1842343_V1_Systeme_alimentation_AD.pdf). The circuit diagram of quadrupoles and bending magnets (June 2021) is included in this git repository under `docs` folder.
- General assembly drawings for the AD are in `AD_LM___0034`, while dogleg injection line 6000 in `PS_LMTTX0275`

### Quad types, circuits and strengths

| Quad type   | Bend angle [rad] | Strength name | Calibration [T/m/A] | Circuit                   |
| ----------- | ---------------- | ------------- | ------------------- | ------------------------- | 
| `QDNEC`     | 0.               |  KDEC         | 4.761/0.7090/1800.0 | QUAD-MAIN-2               |
| `QFNEC`     | 0.               |  KFEC         | 4.761/0.7090/1800.0 | QUAD-MAIN-1 + QUAD-TRIM-4 |
| `QDS`       | 0.               |  KF123        | 4.761/0.7106/1800.0 | QUAD-MAIN-1               |
| `QFNS`      | 0.               | -KF123        | 4.761/0.7106/1800.0 | QUAD-MAIN-1               |
| `QFN`       | 0.030            |  KF4          | 4.488/0.7321/1800   | QUAD-MAIN-1               |
| `QDN`       | 0.018            |  KD5          | 4.097/0.7396/1800   | QUAD-MAIN-1               |
| `QDC`       | 0.018            |  KD53         | 4.18/0.7396/1851.3  | QUAD-MAIN-1               |
| `QFC`       | 0.030            |  KF54         | 4.45/0.7321/1800    | QUAD-MAIN-1 + QUAD-TRIM-5 |
| `QFW6`      | 0.               |  KF6          | 3.93/0.7568/1800    | QUAD-MAIN-1 + QUAD-TRIM-1 |
| `QFW8`      | 0.               |  KF8          | 4.92/0.7494/1800    | QUAD-MAIN-1 + QUAD-TRIM-3 |
| `QDW7`      | 0.               |  KF79         | 4.28/0.7499/1800    | QUAD-MAIN-1 + QUAD-TRIM-2 |
| `QDW9`      | 0.               |  KF9          | 3.38/0.7641/1800    | QUAD-MAIN-1 + QUAD-TRIM-2 |

In principle, some strengths are fixed by geometry of the ring, as some quadrupoles are also used to bend the beam.
See **Parameter list for the Antiproton Accumulator Complex (AAC)**, CERN-PS-95-15-AR-BD, page AC-03:

- QFN type bend of 0.030 rad, offset by +81.6 mm :  KF4 =  0.030/0.0816 =  0.367647;
- QFC type bend of 0.030 rad, offset by +81.6 mm : KF54 =  0.030/0.0816 =  0.367647; 
- QDN type bend of 0.018 rad, offset by -49.6 mm :  KD5 = -0.018/0.0496 = -0.3629;  
- QDC type bend of 0.018 rad, offset by -49.6 mm : KD53 = -0.018/0.0496 = -0.3629;  

but this does not seem to be necessarily the case in our optics, so there might be some leakage orbit by construction.












