/* ************************************************************
 * A few macros to play with AD model ....
 * 
 * All still TODO!!!!
 *
 *
 * ************************************************************ */

compute_dipole_I(beam_p) : macro = {
  BHZ_ANGLE =  .2457993878;
  NEEDED_TM = 3.3356*beam_p*BHZ_ANGLE;

  DR.BHZ_MAIN_I := NEEDED_TM * 2300.78/2.943; 
  DR.BHZ_TRIM_I := NEEDED_TM * 2280/2.92 - DR.BHZ_MAIN_I; 
  ! This current is needed for special S magnet, which is slightly different than others
  DR.BHZTR51.52_I := NEEDED_TM * 2280/2.92 - DR.BHZ_MAIN_I; 

  value, 
    DR.BHZ_MAIN_I,
    DR.BHZ_TRIM_I,
    DR.BHZTR51.52_I; 
};

compute_quadrupole_I(beam_p) : macro = {
  B_rho := 3.3356 * beam_p;
  DR.QUAD_MAIN_1_I := ABS( KF123 ) * B_rho / (4.761/0.7106/1800); 
  DR.QUAD_MAIN_2_I := ABS(  KDEC ) * B_rho / (4.761/0.7090/1800); 
  DR.QUAD_TRIM_1_I := ABS(  KF6  ) * B_rho / (3.93/0.7568/1800) - DR.QUAD_MAIN_1_I; 
  DR.QUAD_TRIM_2_I := ABS(  KF79 ) * B_rho / (4.28/0.7499/1800) - DR.QUAD_MAIN_1_I; 
  DR.QUAD_TRIM_3_I := ABS(   KF8 ) * B_rho / (4.92/0.7494/1800) - DR.QUAD_MAIN_1_I; 
  DR.QUAD_TRIM_4_I := ABS(  KFEC ) * B_rho / (4.761/0.709/1800) - DR.QUAD_MAIN_1_I; 
  DR.QUAD_TRIM_5_I := ABS(  KF54 ) * B_rho / (4.45/0.7321/1800) - DR.QUAD_MAIN_1_I; 
  value,
     DR.QUAD_MAIN_1_I,
     DR.QUAD_MAIN_2_I,
     DR.QUAD_TRIM_1_I,
     DR.QUAD_TRIM_2_I,
     DR.QUAD_TRIM_3_I,  
     DR.QUAD_TRIM_4_I,
     DR.QUAD_TRIM_5_I;
};


compute_scool_phases() : macro = {
  qx = table(summ, q1); 
  qy = table(summ, q2); 
  lring = table(summ, LENGTH);

  ! compute phase advance between monitor and kicher - it should be 0.25
  scool_ph     =  table(twiss,DR.KHM0307, mux) - table(twiss,DR.UHM3107, mux);
  if (scool_ph < 0) {
    scool_ph = scool_ph + qx; 
  }
  ! and the complementary, i.e. from kicker to monitor - it should NOT be 0.25
  scool_ph_bar = qx - scool_ph;
  !
  ! same computation for vertical
  scool_pv     =  table(twiss,DR.KVM0407, muy) - table(twiss,DR.UVM3207, muy);
  if (scool_pv < 0) {
    scool_pv = scool_pv + qy; 
  }
  scool_pv_bar = qy - scool_pv;

  ! computation of distances between pickups and kicker (average)
  scool_l = (table(twiss,DR.KHM0307, S) + table(twiss,DR.KVM0407, S))/2 - (table(twiss,DR.UHM3107, S) + table(twiss,DR.UVM3207, S))/2 ;
  if (scool_l < 0) {
    scool_l = scool_l + lring; 
  }
  scool_l_bar = lring - scool_l;
  
  ! computation of distances between pickups and kicker (H) and (V) seperated
  scool_lh = table(twiss,DR.KHM0307, S) - table(twiss,DR.UHM3107, S) ;
  if (scool_lh < 0) {
    scool_lh = scool_lh + lring; 
  }
  scool_lh_bar = lring - scool_lh;
  scool_lv = table(twiss,DR.KVM0407, S) - table(twiss,DR.UVM3207, S) ;
  if (scool_lv < 0) {
    scool_lv = scool_lv + lring; 
  }
  scool_lv_bar = lring - scool_lv;
  
  ! print everything
  print, text="phase advance monitor->kicker (ph/pv)";
  print, text="phase advance kicker->monitor (ph_bar/pv_bar)";
  print, text="average path length monitor->kicker (l) and kicker->monitor (l_bar)";
  print, text="h/v path length monitor->kicker (lh/lv) and kicker->monitor (lh_bar/lv_bar)";
  value, 
    scool_ph, scool_pv, scool_ph_bar, scool_pv_bar, scool_l, scool_l_bar, scool_lh, scool_lh_bar, scool_lv, scool_lv_bar; 
  
  ! print also Twiss functions
  value, table(twiss,DR.KHM0307, betx), table(twiss,DR.KHM0307, bety);
  value, table(twiss,DR.KVM0407, betx), table(twiss,DR.KVM0407, bety);
  value, table(twiss,DR.UHM3107, betx), table(twiss,DR.UHM3107, bety);
  value, table(twiss,DR.UVM3207, betx), table(twiss,DR.UVM3207, bety);

};


!split_quad(QUADNAME) : marco = {
!  QUADNAME = DR.QFN02;
!  AUX.ELM.AT = QUADNAME->AT;
!AUX.ELM.L  = DR.QFN02->l;
!value, DR.QFN02:1->at; 
!DUMPSEQU, SEQUENCE=string, LEVEL=integer;
!DUMPSEQU, SEQUENCE=ad, level=1;
!
!  SEQEDIT,SEQUENCE=AD;
!  REMOVE,  ELEMENT=QUADNAME;
!INSTALL, ELEMENT=QFNS.H,                , at = AUX.ELM.AT - AUX.ELM.L/2 + 0.177650;
!INSTALL, ELEMENT=DR.HVCOR02            , at = AUX.ELM.AT - AUX.ELM.L/2 + 0.355300;
!INSTALL, ELEMENT=QFNS.H,                , at = AUX.ELM.AT - AUX.ELM.L/2 + 0.532950;
!ENDEDIT;
!
!}


match_tunes(qx, qy, dqx, dqy) : macro = {
  KF4   := R1*KF123;
  KD5   := R2*KF123;
  KF9   := R3*KF79;
  KD53  := R2*KF123;
  KF54  := R1*KF123;
  match, sequence=ad;
          vary, name = KF123, step=0.00001;
          vary, name = KF6, step=0.00001;
          vary, name = KF79, step=0.00001;
          vary, name = KF8, step=0.00001;
          vary, name = KDEC, step=0.00001;
          vary, name = KFEC, step=0.00001;
          constraint, range = #E, mux = qx + dqx, muy = qy + dqy;
  jacobian,calls=50000,bisec=3, tolerance = 1E-20;
  ENDMATCH;

};
